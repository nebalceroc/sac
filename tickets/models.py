from __future__ import unicode_literals
from django.utils import timezone
from django.db import models


class Ticket(models.Model):
	user = models.CharField(max_length=20, null=False)
	title = models.TextField(default="empty", max_length = 30)
	message	= models.TextField(default="empty", max_length = 500)

	statuses = (
        ('0', 'created'),
        ('1', 'active'),
        ('2', 'solved'),
        ('3', 'problematic'),
        )
	
	status = models.CharField(max_length=1, choices=statuses, default='0')
	time_created = models.DateTimeField(default = timezone.now)

        class Meta:
                verbose_name = "Ticket"
                verbose_name_plural = "Tickets"

        def __unicode__(self):
                return "Ticket - %s" % (self.id)