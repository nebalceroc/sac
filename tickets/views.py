from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponseBadRequest, HttpResponse

# Create your views here.
def index(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect("user_panel/")

	context = {'key':'value',}
	return render(request, 'index.html', context)

def user_panel(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'user_panel.html', context)

def model_panel(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'model_panel.html', context)

def model_panel2(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'model_panel2.html', context)

def dashboard_panel(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'dashboard_panel.html', context)

def charts_panel(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'charts_panel.html', context)

def config_panel(request):
	context = {'key':'value',                                   
                            }
	return render(request, 'config_panel.html', context)