from django.conf.urls import patterns, url, include
from views import *
from rest_framework import routers
from account import views as account_views

#routers for the rest framework
router = routers.DefaultRouter()

urlpatterns = [
	url(r'^user_panel/model_panel/',model_panel),
	url(r'^user_panel/model_pane12/',model_panel2),
	url(r'^user_panel/dashboard_panel/',dashboard_panel),
	url(r'^user_panel/charts_panel/',charts_panel),
	url(r'^user_panel/config_panel/',config_panel),
	url(r'^user_panel/logout/?$',account_views.view_logout),
	url(r'^user_panel/',user_panel),
    url(r'^api/', include(router.urls)),
    url(r'^$', index),
]